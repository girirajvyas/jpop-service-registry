package com.epam.jpop.serviceregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class JpopServiceRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpopServiceRegistryApplication.class, args);
	}

}
